<?php
include ("../../../../links.php"); 
include ("../../../../includes/header.html"); 
?>

   <fieldset> <h1>Java HDF Interface (JHI)</h1></fieldset>
<p>
<h2>
1. What it is</h2>
The <b>Java HDF Interface (JHI)</b> is a Java package 
(<a href="https://www.hdfgroup.org/products/java/hdf-java-html/javadocs/ncsa/hdf/hdflib/package-summary.html">hdf.hdflib</a>) 
that ``wraps'' the HDF-4 library*. The JHI may be used by any Java
application that needs to access HDF-4 files. This product cannot be used
in most network browsers because it accesses the local disk using native
code.
<center><table BORDER=1 bgcolor="#FFFFCC" >
<tr>
<td><b>Note:</b> The JHI does not support HDF-5. See the
<a href="../jhi5/index.html">JHI5</a>.</td>
</tr>
</table></center>

<p>A central part of the JHI is the Java class <i>
<a href="https://www.hdfgroup.org/products/java/hdf-java-html/javadocs/ncsa/hdf/hdflib/HDFLibrary.html">hdf.hdflib.HDFLibrary</a></i>.
The <i>HDFLibrary </i>class calls the standard (<i>i.e.</i>, `native' code)
HDF library, with native methods for most of the HDF functions.
<p>It is extremely important to emphasize that <i>this package is not a
pure Java implementation of the HDF library.</i> The JHI calls the same
HDF library that is used by C or FORTRAN programs.
<p>The Java HDF Interface consists of Java classes and dynamically linked
native libraries. The Java classes declare <i>static native</i> methods,
and the library contains C functions which implement the native methods.
The C functions call the standard HDF library, which is linked as part
of the same library on most platforms.<hr>
<h3>
Intended purpose</h3>
The Java HDF Interface is intended to be the standard interface for accessing
HDF-4 files from Java programs. The JHI is a foundation upon which application
classes can be built. All classes that use this interface package should
interoperate easily on any platform that has HDF-4 installed.
<p>It is unlikely that Java programs will want to directly call the HDF
library. More likely, data will be represented by Java classes that meet
the needs of the application. These classes can implement methods to store
and retrieve data from HDF files using the Java HDF library.<hr>
<h3>
Important Changes</h3>

<center><table BORDER=1 bgcolor="#FFFFCC">
<tr>
<td><b>Very Important Change:</b> Version 2.6 (and above) of the JHI declares
all HDF library
<br>calls as "static native" methods, and loads the library with a 'static'
constructor.
<br>Source code which used earlier versions of the JHI should be changed
to
<br>reflect this new implementation.</td>
</tr>
</table></center>

<p>Other changes include:
<ul class="ul">
<li class="li2">
Support for Macintosh and PC path names, e.g., in <i>Hopen()</i>.</li>

<li class="li2">
A separate distribution of the JHI. This contains everything from the full
source distribution, omitting all the packages not used by the JHI.</li>

<li class="li2">
The <b>HDFArray</b> class was extended to support more types of Java arrays,
including arrays of any sub-class of <b>Number </b>(<b>Integer</b>, <b>Float</b>,
etc.) and arrays of <b>String</b>.</li>
</ul>
<hr>
<h2>
2. How to use it</h2>
For example, the HDF library had the function <b>Hopen</b> to open an HDF
file. The Java interface is the class <i>
<a href="https://www.hdfgroup.org/products/java/hdf-java-html/javadocs/ncsa/hdf/hdflib/HDFLibrary.html">hdf.hdflib.HDFLibrary</a></i>,
which has a method:
<pre><b>static</b> <b>native int Hopen(String filename, int access);</b></pre>
The native method is implemented in C using the
<!-- <a href="http://java.sun.com/j2se/1.3/docs/guide/jni/index.html"> -->
Java Native Method Interface (JNI). This is written something like the following: <pre><b>JNIEXPORT jint
JNICALL Java_hdf_hdflib_HDFLibrary_Hopen
(JNIEnv *env,
jclass class,
jstring hdfFile,
jint access)
{

/* ... */

/* call the HDF library */
retVal = Hopen((char *)file, access, 0);

/* ... */
}</b></pre>
This C function calls the HDF library and returns the result appropriately.
<p>There is one native method for each HDF entry point (several hundred
in all), which are compiled with the HDF library into a dynamically loaded
library (<i>libhdf</i>). Note that this library must be built for each
platform.
<p>To call the HDF `<i>Hopen</i>' function, a Java program would import
the package '<i>hdf.hdflib.*'</i>, and then invoke the '<i>Hopen</i>'
method on the object '<i>HDFLibrary</i>'. The Java program would look something
like this:
<pre><b>import hdf.hdflib.*;

{
/* ... */

HDFLibrary.Hopen("myFile.hdf", access);

/* ... */
}</b></pre>
The <i>HDFLibrary </i>class automatically loads the HDF-4 library with
the native method implementations and the HDF-4 library.<hr>
<h2>
3. Technical notes</h2>
The following is a list of a few important technical notes of the JHI.
For more details, please read <a href="jhi-design.html">JHI Design Notes</a>.
<p>
<center><table BORDER=1 bgcolor="#FFFFCC" >
<tr>
<td><b>Very Important Change:</b> Please note that Version 2.6 (and above)
of the JHI
<br>declares all HDF library calls as "static native" methods, and loads
the library with
<br>a 'static' constructor. Source code which used earlier versions of
the JHI
<br>should be changed to reflect this new, more efficient implementation.</td>
</tr>
</table></center>
<p>
<ul class="ul">
<li class="li2">
<b>Data conversion and copying.</b> The Java HDF Interface must translate
data between C data types and Java data types. For instance, when a Java
program reads a two dimensional array of floating point numbers (<b>float
[][]</b>), the HDF native library actually returns an array of bytes. The
Java HDF Interface converts this to the correct Java object, and returns
that to the calling program. This process uses the Java Core Reflection
package to discover the type of the array, and then calls native code routines
to convert the bytes from native (C) order to the correct Java object(s).
This data conversion is platform specific and clearly adds overhead to
the program.</li>

<li class="li2">
<b>The JHI Interface to HDF.</b> The Java HDF interface follows the HDF
C interface as closely as possible. However, Java does not support pass-by-reference
parameters, so all parameters that must be returned to the caller require
special treatment for Java. In general, such parameters are passed as elements
of an array. For example, to return an integer parameter, the Java native
method would be declared to use an integer array. For instance, the C
function</li>

<pre><b>void foo( int inVar /* IN */, int *outVar /* OUT */ )</b></pre>
would be rendered in Java as:
<pre><b>static public native void foo( int inVar, int []outVar )</b></pre>
where the value of 'outVar' would be returned as 'outVar[0]'.
<li class="li2">
<b>Exceptions.</b> The JHI declares all the methods of HDF to throw Java
exceptions of type `<i>HDFLibraryException</i>'. Errors returned by the
native HDF library will be detected and the appropriate <i>Exception </i>raised,
just as from regular Java methods. There is no need for the Java program
to analyze HDF library error codes or return values. However, in this release
many HDF library errors are not detected as exceptions, so exceptions will
not be raised except when opening a file. Error handling will be completed
in a future release.</li>
</ul><hr>

<h2>
<a NAME="DOWNLOAD"></a>4. To Obtain</h2>
The JHI is included with the HDF-Java Products, which can be downloaded
from the <a href="http://www.hdfgroup.org/products/java/index.html">HDF-Java Home Page</a>.

<?php
include ("../../../../includes/footer.html"); 
?>
